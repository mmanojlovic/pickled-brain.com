# Whats runing on pickled-brain.com #

Installed components:

## Bower package Manager ##

[http://bower.io/](http://bower.io/)

### Config file for Bower ###

```
#!bower.json

bower.json
```

To be used from root folder with cli:

```
#!bower.json

bower info
```

### Libs added via bower ###
1. jquery
2. jquery.lazyload
3. leaflet
4. angular
5. font-awesome-bower
6. bootstrap-sass-official
7. font-awesome-sass

## Grunt Task Runner ##
[http://gruntjs.com/](http://gruntjs.com/)

### Config file for Grunt###
```
#!gruntfile.js
gruntfile.js
```

### Grunt Tasks ###
1. uglify - minification of all js files used from bower folder
2. imagemin - minification of all images from assets/images_raw to images folder

To be used from root folder with cli:

```
#!bower.json

grunt watch
grunt
```

## SCSS With Compass ##

[http://compass-style.org/](http://compass-style.org/)

Folders:
assets/sass

### Config file for Grunt###
```
#!gruntfile.js
config.rb
```