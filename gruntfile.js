/*
*     Grunt task runner
*/

module.exports = function(grunt) {
  var mozjpeg = require('imagemin-mozjpeg');
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    // uglify aka minify
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: [
            '<banner>',
            // Installed
            "bower_components/jquery/dist/jquery.min.js",
            "bower_components/bootstrap-sass-official/assets/javascripts/bootstrap.js",
            "bower_components/jquery.lazyload/jquery.lazyload.min.js",
            "bower_components/leaflet/dist/leaflet.js",
            "bower_components/angular/angular.min.js",
            "bower_components/angular-translate/angular-translate.min.js",
            //Custom js
            "assets/js/ekko-lightbox.js",
            "assets/js/jquery.mobile.custom.min.js"
        ],
        dest: 'js/minified.js'
      }
    },
    // minify images
    imagemin: {                          // Task
        dynamic: {                         // Another target
         options: {                       // Target options
            optimizationLevel: 3,
            svgoPlugins: [{ removeViewBox: false }],
            use: [mozjpeg()]
          },
          files: [{
            expand: true,                  // Enable dynamic expansion
            cwd: 'assets/images_raw',                   // Src matches are relative to this path
            src: ['**/*.{png,jpg,gif,svg}'],   // Actual patterns to match
            dest: 'images/'                  // Destination path prefix
          }]
        }
      }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // minify images
  grunt.loadNpmTasks('grunt-contrib-imagemin');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);
  grunt.registerTask('default', ['imagemin']);

};


