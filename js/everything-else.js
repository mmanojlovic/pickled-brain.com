// create a map in the "map" div, set the view to a given place and zoom
var map = L.map('map').setView([44.819406, 20.452809], 16);

// add an OpenStreetMap tile layer
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

// add a marker in the given location, attach some popup content to it and open the popup

var LeafIcon = L.Icon.extend({
    options: {
        shadowUrl: 'images/map-icon-shadow.png',
        iconSize:     [60, 96],
        shadowSize:   [102, 130],
        iconAnchor:   [30, 96],
        shadowAnchor: [25, 120],
        popupAnchor:  [-3, -76]
    }
});

var greenIcon = new LeafIcon({iconUrl: 'images/map-icon.png'})
L.marker([44.819406, 20.452809], {icon: greenIcon}).bindPopup("We are located here, come visit us").addTo(map);


//background cycle
var currentBackground = 0;
var backgrounds = [];
backgrounds[0] = 'images/bg1.jpg';
backgrounds[1] = 'images/bg2.jpg';
backgrounds[2] = 'images/bg3.jpg';

function changeBackground() {
    currentBackground++;
    if(currentBackground > 2) currentBackground = 0;

    $('#bg-fade').fadeOut(100,function() {
        $('#bg-fade').css({
            'background' : "url('" + backgrounds[currentBackground] + "') no-repeat center center"
        });
        $('#bg-fade').fadeIn(100);
    });


    setTimeout(changeBackground, 3000);
}

// wweeeeee document loaded
$(document).ready(function ($) {
    setTimeout(changeBackground, 3000);

    // Mobile navigation on images
    $("body").swiperight(function() {  
        $('.glyphicon-chevron-left').trigger( "click" );
        return console.log('swiped right');
    });  
    $("body").swipeleft(function() {  
        $('.glyphicon-chevron-right').trigger( "click" );
        return console.log('swiped left');
    });  

    $(function() {
        $("img.img-responsive").lazyload({
            effect : "fadeIn",
            delay: 5000,
            effectTime: 500
        });
    });
    // delegate calls to data-toggle="lightbox"
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();

        return $(this).ekkoLightbox({
            onShown: function() {
                FB.XFBML.parse(document.getElementById('social-fb'));
                if (window.console) {
                    return console.log('Checking our the events huh?');
                }
            },
            onNavigate: function(direction, itemIndex) {
                if (window.console) {
                    return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                }
            }
        });
    });
});