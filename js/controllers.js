    // Set galery controler
    app.controller('GaleryControler', function(){
       this.products = images;
    });

        app.controller('AppCtrl',['$scope', function($scope){
           var show = false;
           $scope.on = function(){
             show = true;
           }
           $scope.off = function(){
             show = false;
           } 
           $scope.showButton = function(){
             return show;
           }
        }]);


        // Set image
        var images = [
            {
                name: 'Clocks and roses',
                src: 'images/galery/tattoo-clock2-pickled-brain.jpg',
                url: 'images/galery/tattoo-clock2-pickled-brain-thumb.jpg',
            },
            {
                name: 'Dog',
                src: 'images/galery/tattoo-dog-pickled-brain.jpg',
                url: 'images/galery/tattoo-dog-pickled-brain-thumb.jpg',
            },
            {
                name: 'Flowers',
                src: 'images/galery/tattoo-flowers-pickled-brain.jpg',
                url: 'images/galery/tattoo-flowers-pickled-brain-thumb.jpg',
            },
            {
                name: 'Girl portrait',
                src: 'images/galery/tattoo-girl-style-pickled-brain.jpg',
                url: 'images/galery/tattoo-girl-style-pickled-brain-thumb.jpg',
            },
            {
                name: 'Prince Mihailo Monument',
                src: 'images/galery/tattoo-horse-pickled-brain.jpg',
                url: 'images/galery/tattoo-horse-pickled-brain-thumb.jpg',
            },
            {
                name: 'Clock',
                src: 'images/galery/tattoo-clock3-pickled-brain.jpg',
                url: 'images/galery/tattoo-clock3-pickled-brain-thumb.jpg',
            },
            {
                name: 'Japanese style tattoo',
                src: 'images/galery/tattoo-japanese-style-pickled-brain.jpg',
                url: 'images/galery/tattoo-japanese-style-pickled-brain-thumb.jpg',
            },
            {
                name: 'Girl and Lion',
                src: 'images/galery/tattoo-lion-girl-pickled-brain.jpg',
                url: 'images/galery/tattoo-lion-girl-pickled-brain-thumb.jpg',
            },
            {
                name: 'Portrait',
                src: 'images/galery/tattoo-portrait-pickled-brain.jpg',
                url: 'images/galery/tattoo-portrait-pickled-brain-thumb.jpg',
            },
            {
                name: 'Mexican style tattoo',
                src: 'images/galery/tattoo-mexican-death-festival-pickled-brain.jpg',
                url: 'images/galery/tattoo-mexican-death-festival-pickled-brain-thumb.jpg',
            },

            {
                name: 'Rose',
                src: 'images/galery/tattoo-rose-pickled-brain.jpg',
                url: 'images/galery/tattoo-rose-pickled-brain-thumb.jpg',
            },
            {
                name: 'Rose',
                src: 'images/galery/tattoo-rose2-pickled-brain.jpg',
                url: 'images/galery/tattoo-rose2-pickled-brain-thumb.jpg',
            },
            {
                name: 'Signs',
                src: 'images/galery/tattoo-signs-pickled-brain.jpg',
                url: 'images/galery/tattoo-signs-pickled-brain-thumb.jpg',
            },
            {
                name: 'Sleeve',
                src: 'images/galery/tattoo-sleeve-pickled-brain.jpg',
                url: 'images/galery/tattoo-sleeve-pickled-brain-thumb.jpg',
            },
            {
                name: 'Wolf',
                src: 'images/galery/tattoo-wolf-pickled-brain.jpg',
                url: 'images/galery/tattoo-wolf-pickled-brain-thumb.jpg',
            },
            {
                name: 'Demon Hunter Sleve',
                src: 'images/galery/tattoo-demon-hunter-sleve-pickled-brain.jpg',
                url: 'images/galery/tattoo-demon-hunter-sleve-pickled-brain-thumb.jpg',
            },
            {
                name: 'Clown',
                src: 'images/galery/tattoo-pupetmaster-clown-pickled-brain.jpg',
                url: 'images/galery/tattoo-pupetmaster-clown-pickled-brain-thumb.jpg',
            },
            {
                name: 'Lion in all its glory',
                src: 'images/galery/tattoo-lion-head-pickled-brain.jpg',
                url: 'images/galery/tattoo-lion-head-pickled-brain-thumb.jpg',
            },
            {
                name: 'Clock',
                src: 'images/galery/tattoo-clock-pickled-brain.jpg',
                url: 'images/galery/tattoo-clock-pickled-brain-thumb.jpg',
            },
            {
                name: 'Red nosed clown',
                src: 'images/galery/tattoo-clown-red-nose-pickled-brain.jpg',
                url: 'images/galery/tattoo-clown-red-nose-pickled-brain-thumb.jpg',
            },
            {
                name: 'Blind Skateboards Logo on a quest of a better lamppost',
                src: 'images/galery/tattoo-blind-skateboards-pickled-brain.jpg',
                url: 'images/galery/tattoo-blind-skateboards-pickled-brain-thumb.jpg',
            },
            {
                name: 'Phoenix on the back',
                src: 'images/galery/tattoo-phoenix-on-the-back-pickled-brain.jpg',
                url: 'images/galery/tattoo-phoenix-on-the-back-pickled-brain-thumb.jpg',
            },
            {
                name: 'Phoenix in the flight pose',
                src: 'images/galery/tattoo-phoenix-flying-pickled-brain.jpg',
                url: 'images/galery/tattoo-phoenix-flying-pickled-brain-thumb.jpg',
            },
            {
                name: 'Some random caligraphy letters, got to have those',
                src: 'images/galery/tattoo-caliography-pickled-brain.jpg',
                url: 'images/galery/tattoo-caliography-pickled-brain-thumb.jpg',
            },

            {
                name: 'Sad/Happy Mask',
                src: 'images/galery/tattoo-sad-mask-happy-mask-pickled-brain.jpg',
                url: 'images/galery/tattoo-sad-mask-happy-mask-pickled-brain-thumb.jpg',
            },
            {
                name: 'Simple Anchor',
                src: 'images/galery/tattoo-anchor-pickled-brain.jpg',
                url: 'images/galery/tattoo-anchor-pickled-brain-thumb.jpg',
            },
            {
                name: 'Tedy Bear #happy',
                src: 'images/galery/tattoo-teddy-bear-pickled-brain.jpg',
                url: 'images/galery/tattoo-teddy-bear-pickled-brain-thumb.jpg',
            }

        ];



    // Contact controller
    app.controller('ContactController', function ($scope, $http) {
    $scope.result = 'hidden'
    $scope.resultMessage;
    $scope.formData; //formData is an object holding the name, email, subject, and message
    $scope.submitButtonDisabled = false;
    $scope.submitted = false; //used so that form errors are shown only after the form has been submitted
    $scope.submit = function(contactform) {
        $scope.submitted = true;
        $scope.submitButtonDisabled = true;
        if (contactform.$valid) {
            $http({
                method  : 'POST',
                url     : 'contact-form.php',
                data    : $.param($scope.formData),  //param method from jQuery
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  //set the headers so angular passing info as form data (not request payload)
            }).success(function(data){
                console.log(data);
                if (data.success) { //success comes from the return json object
                    $scope.submitButtonDisabled = true;
                    $scope.resultMessage = data.message;
                    $scope.result='bg-success';
                } else {
                    $scope.submitButtonDisabled = false;
                    $scope.resultMessage = data.message;
                    $scope.result='bg-danger';
                }
            });
        } else {
            $scope.submitButtonDisabled = false;
            $scope.resultMessage = 'Failed :( Please fill out all the fields.';
            $scope.result='bg-danger';
        }
    }
});