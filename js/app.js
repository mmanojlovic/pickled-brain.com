var app = angular.module('galery', ['pascalprecht.translate']);

app.config(function ($translateProvider) {
  $translateProvider.translations('en', {
  	// Contact
    Contact_us_title: 'Contact Us',
    Contact_us_text: 'Use contact form on a right to get it touch. If you are really in a hurry or close to us, give us a call via phone',
    Contact_us_workh: 'WORK HOURS:',
    Contact_us_workh_line1: 'Monday - Friday : 11am - 19pm',
    Contact_us_workh_line2: 'Saturday/Sunday: if needed, call us',
    // Contact form
    Contact_us_form_name: 'Your Name',
    Contact_us_form_email: 'Your Email',
    Contact_us_form_message: 'Your message...',
    Contact_us_form_button: 'Send Message',
    //About section
    About_header: 'Pickled Brain Tattoo is Sanja Tomasevic',
    About_text1: 'Pickled Brain Tattoo studio is Sanja Tomašević, born 1987. She is into tattoos for the last 10 years, and for the last 3 years - by profession. ',
    About_text2: 'There is a rich and versatile portfolio behind her, and you can get a glimps of it checking out this website (gallery above), or facebook page linked below.',
    About_text3: 'Cheers, and see you at the studio',
    // Gallery
    Supply_title: 'We use',

  });
  $translateProvider.translations('sr', {
  	// Contact
    Contact_us_title: 'Kontaktirajte nas',
    Contact_us_text: 'Iskoristite formu na desnoj strani da stupite u kontakt. Ako ste u žurbi ili ste blizu nas i hocete da svratite, pozovite nas preko telefonskog broja koji je ispisan ispod.',
    Contact_us_workh: 'RADNO VREME:',
    Contact_us_workh_line1: 'Ponedeljak - Petak : 11am - 19pm',
    Contact_us_workh_line2: 'Subota/Nedelja: po potrebi, zakažite.',
    // Contact form
    Contact_us_form_name: 'Vaše Ime',
    Contact_us_form_email: 'Vaš email',
    Contact_us_form_message: 'Poruka...',
    Contact_us_form_button: 'Pošalji',
    //About section
    About_header: 'Pickled Brain Tattoo je Sanja Tomasević',
    About_text1: 'Pickled Brain Tattoo studio je Sanja Tomašević rodjena 1987. godine, tetoviranjem se profesionalno bavi zadnjih nekoliko godina. ',
    About_text2: 'Iza sebe ima veliki broj tetovaža različite tematike, i dalje pokušavamo da sastavimo listu ljudi koji su uradili tetovaže kako bi mogli da ih slikamo i stavimo u album :) ',
    About_text3: 'Pozdrav i vidimo se u studiju.',
      Supply_title: 'Koristimo',
  });
  $translateProvider.preferredLanguage('en');
});

app.controller('Ctrl', function ($scope, $translate) {
  $scope.changeLanguage = function (key) {
    $translate.use(key);
  };
});


